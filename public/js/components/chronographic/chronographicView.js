/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.04 + 2021.09 + 2022.03
 * @origin :: Belgium, EU
 */

/**
 * Manage Chrono-Graphic
 * @param {Object} reseter - HTML element of infography
 * @param {Array} togglers - HTML elements List of periods
 */
 class ChronographicView {
     
    constructor(container, periods) {
		this._chronographic = container;
		this._periods = periods;

		if (window.innerWidth > 680) {
			this._buildLadder();
			if (window.matchMedia('(any-hover:hover)').matches) this._enable();
		}
	}

	/**
	 * Build Infographic w/ date rungs and sets periods
	 * [!] This function must be called once. When and if screen-sizes match
	 */
	_buildLadder() {
		let begin = 500; // Ladder begins at 500 after J.C.
		let end = 2000; // Ladder ends at 2000 after J.C.
		let space = 100; // Ladder is spaced by 100 years

		// Add date on Chronologic Ladder
		for (; end >= begin ; end = end-space) {
			this._chronographic.insertAdjacentHTML('afterbegin', `<span class="year" style="top:${end - begin}px"><strong>${end}</strong></span>`);
		}

		// Position of each period on Chronologic Ladder
		this._periods.forEach( period => {
			period.style.top = `${period.getAttribute('data-begin') - begin}px`;
			period.style.height = `${period.getAttribute('data-delay')}px`;
		});

		// Enable CSS
		this._chronographic.classList.add('ladder');
	}

	/**
	 * Enable asynchronous requests
	 */
	_enable() {
		this._periods.forEach( period => {
			period.addEventListener('mouseover', this._implementList);
			period.addEventListener('mousemove', this._positionList);
		});
		this._chronographic.classList.add('ready');
	}

	/**
	 * Load Compositors from targeted Period
	 * @param {Object} evt - Event reference period
	 */
	_implementList = async evt => {
		let target = evt.currentTarget // [!]

		// Disable future Request from same target
		target.removeEventListener('mouseover', this._implementList);

		// Targeting Resources
		let tag = target.getAttribute('data-tag');

		const data = await fetch( `/async/chronographic/${tag}` ).then( this._status ).then( this._build ).catch( this._notFound );
		
		// Insertion of data
		target.insertAdjacentHTML('beforeend', data);
	}

	/**
     * Identify status of response
     * @param {string} res 
     * @returns {string}
     */
	_status = res => {
        if (res.status < 200 || res.status >= 400) throw new Error('Cible non trouvée');
        return res;
    }

	/**
     * Build HTML list with results
     * @param {string} res 
     * @returns {string}
     */
	_build = async res => { 
		let results = await res.json();
		let total = results.length;

		let list = `<ul class="list">`;
		for ( let i = 0 ; i < total ; i++) list += `<li>${results[i].firstname} ${results[i].lastname}</li>`;
		list += `</ul>`;

		return list;
	}

	/**
     * Handle error and build HTML response to implement
     * @param {Object} err 
     * @returns 
     */
	_notFound = err => `<ul><li class="nothing"><strong>Erreur :</strong> ${err.message}</li></ul>`;

	/**
	 * Position the compositors' list when mouving the mouse
	 * @param {Object} evt - Mouse event
	 */
	_positionList = evt => {
		evt.currentTarget.lastElementChild.style.bottom = (evt.currentTarget.offsetHeight - evt.offsetY) +'px';
	}

	/**
     * Switch layouting of infographic when window is resizing and innerWidth < 680 px.
     */
	autoLayout = () => {
        if (window.innerWidth < 680) {
            this._chronographic.classList.remove('ladder', 'ready');
        } else {
            this._chronographic.classList.add('ladder');
			if (window.matchMedia(`(any-hover:hover)`).matches) {
				this._chronographic.classList.add('ready');
			} else {
				this._chronographic.classList.remove('ladder');
			}
        }
    }
}

export default ChronographicView;