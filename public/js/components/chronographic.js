'use strict';
/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.04 + 2021.09 + 2022.03
 * @origin :: Belgium, EU
 */

import ChronographicView from './chronographic/chronographicView.js';
import GUIServices from '../modules/guiServices.js';

(() => {
    const container = document.querySelector('#chronographic');
    const periods = document.querySelectorAll('#chronographic > div');
    
    const chronographic = new ChronographicView(container, periods);

    // Attach Resizing Analysis
    GUIServices.delayedResizer(chronographic.autoLayout);
})();