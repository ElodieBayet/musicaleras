'use strict';
/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.10 + 2021.06 + 2021.12
 * @origin :: Belgium, EU
 */

import SkillsFilterView from './items-filter/itemsFilterView.js';

(() => {
    const togglers = document.querySelectorAll('.filters a.toggle');
    const reseter = document.querySelector('.filters a.anchor');
    const targets = document.querySelectorAll('.topic');

    const itemsFilter = new SkillsFilterView(togglers, reseter, targets);
})();