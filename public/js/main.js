'use strict';
/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.04 + 2021.09
 * @origin :: Belgium, EU
 */

/** @file Main Program for Modules – 'defer' loaded src */

/**
 * Cross-Page requirements and management
 */
import GUIServices from './modules/guiServices.js';
import Opener from './modules/opener.js';

( () => {
	const _NAV = {
		container: document.querySelector('#uihead'), 
		trigger: document.querySelector('#uihead button'), 
		receptor: document.querySelector('#uihead .navigation')
	}
	
	const menu = GUIServices.implement( _NAV, Opener ) || GUIServices.moduleError('Menu');
	
	if ( menu instanceof Opener ){
		GUIServices.delayedResizer( menu.autoCompute );
	}
})();


// Remove class '.nojs'
document.documentElement.removeAttribute('class');