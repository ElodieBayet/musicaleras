# MusicalEras :: Classical Music history

Date | Reviewed | Version | Purpose | Live
---- | -------- | ------- | ------- | ----
2018.01 | 2022.04 | 1.1.0 | Demo | [MusicalEras](https://musicaleras.herokuapp.com/)


## Sommaire 
0. [Presentation](#markdown-header-0-presentation)
    * [0.0 - Technologies](#markdown-header-00-technologies)
    * [0.1 - Features](#markdown-header-01-features)
    * [0.2 - Usage](#markdown-header-02-usage)
1. [Structure](#markdown-header-1-structure)
    * [1.0 - SEO Architecture](#markdown-header-10-seo-architecture)
    * [1.1 - Folders and files](#markdown-header-11-folders-and-files)
    * [1.2 - Database](#markdown-header-12-database)
    * [1.3 - Interface](#markdown-header-13-interface)
2. [Development](#markdown-header-2-development)
3. [Remarks](#markdown-header-3-remarks)

---

## 0 - Presentation

Historical inventory of eras and compositors in “Classical Music”. A timeline of periods throughout past centuries, and each period lists its most famous compositors.

**Important** - This applicatiion is a simple demo. It's not a complete historical reference, and remains very approximate. The main source is **[Wikipédia : Chronologie de la musique classique occidentale](https://fr.wikipedia.org/wiki/Chronologie_de_la_musique_classique_occidentale)**.


### 0.0 - Technologies

* [Node.js](https://nodejs.org/en/docs/)
* [NPM](https://www.npmjs.com/)
* [MongoDB](https://www.mongodb.com/docs/manual/)
* [Pug](https://pugjs.org/api/getting-started.html)


### 0.1 - Features

* **Asynchrounous http request**
* **Chronological infography** 
* **“Bypass” navigation** : _automatic shortcuts to previous and next periods or compositors_
* **Compositors' filter** : _hide or display compositors list by their period_
* **List of periods**
* **List of compositors**


### 0.2 - Usage

This project is just a _demo_. It doesn't fit for public or professionnal usage. Reproduction, public communication, partial or entire of this Website or its content are strictly prohibited without an official and written authorization by the Developer.

---

## 1 - Structure

### 1.0 - SEO Architecture

#### URIs

* Home, with “chronographic” :: `./` 
    - Sub-list of compositors sorted by period :: `./async/chronographic/:tag`
* Periods :: `./periodes`
    - {Title of the period} :: `./periodes/:tag`
* Compositors :: `./compositeurs`
    - {Compositor's firstname and last name} :: `./compositeurs/:tag`
* Informations :: `./infos`

#### Schéma

![Website Tree](/assets/figures/seo_architecture.jpg)


### 1.1 - Folders and Files

* `_mongodb/` _collections for Database implementation_
* `assets/` _bank of static resources_
* `core/` _backend source code of application_
    - `controllers/`
    - `library/` _tools and utilities_
    - `models/`
    - `routes/`
    - `services/` _middlewares for common services_
    - `views/` _templates for Pug rendering_
        - `components/`
        - `contents/`
        - `layout.pug`
    - `app.js`
* `public/` _frontend source code distributed for production_
* `nodemon.json` _nodemon configuration for development mode_
* `server.js` _start and entry file_


### 1.2 - Database

The `tag` field is used for slug. Association is `Many-to-Many` through `compositorSchema`.

### Entities

```js
const periodSchema = {
    tag: { 
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    name: { 
        type: String,
        required: true,
        minLength: 3,
        maxLength: 128
    },
    begin: { 
        type: Number,
        required: true,
        min: 1, max: 2100
    },
    end: {
        type: Number,
        required: true,
        min: 1, max: 2100
    },
    description: {
        type: String,
        maxLength: 500
    }
}
```

```js
const compositorSchema = {
    tag: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    lastname: {
        type: String,
        required: true,
        minLength: 3,
        maxLength: 128,
        uppercase: true
    },
    firstname: {
        type: String,
        required: true,
        minLength: 3,
        maxLength: 128
    },
    birth: {
        type: Date,
        required: true
    },
    death: {
        type: Date,
        required: true
    },
    origin: {
        type: String,
        maxLength: 128
    },
    figure: {
        type: String,
        default: '/figures/default_640x640.jpg'
    },
    period: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Periods'
        }
    ] 
}
```

### 1.4 - Interface

#### Front-End Template

* [Neptune](https://demo.elodiebayet.com/neptune/base.html)

#### Mockups

![Mockup Wide #1](/assets/figures/mockup_wide_01.jpg)

---

## 2 - Development

Architecture is _Model-View-Controller_. Prototype Oriented Programming.


### 2.0 - NPM Packages
```json
"dependencies": {
    "async": "^3.2.3", // handle async multiple sud-queries in database
    "compression": "^1.7.4", // Gzip compression of responses
    "express": "^4.17.3", // Application framework
    "helmet": "^4.6.0", // Secure Express application by setting HTTP headers
    "mongoose": "^6.3.0", // MongoDB ODM
    "pug": "^3.0.2" // Template engin for HTML rendering
},
"devDependencies": {
    "env-cmd": "^10.1.0", // Load environment variables on start-up
    "nodemon": "^2.0.15" // Monitor for modification during development
}
```

---

## 3 - Remarks

None.