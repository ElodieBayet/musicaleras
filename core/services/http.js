'use strict';
const debug = require('debug')('core:http');
const { HttpException } = require('../library/httpException');

/**
 * @file Middlewares for Http services : handling request, headers, etc.
 */

/**
 * Catch any request that doesn't match a route
 * @param {*} next Transmit an HttpException
 */
exports.badRequest = (req, res, next) => {
    debug(`[!] - Request can't be completed`);

    const error = new HttpException(400, 'Bad Request');
    next(error);
}

/**
 * Fallout for any bad requests or controller errors
 * @param {HttpException} error Caught HttpException
 * @param {*} res Http response
 * @returns View render of error page
 */
exports.fallout = (error, req, res, next) => {
    
    res.locals.route = 'error'; 
    res.locals.uri = '';

    const status = error.status || 500;
    const message = error.message || 'Internal Application Error';

    debug(`[!] - Error ${status} :: ${message}`);

    // Render Error page
    return res.status(status).render('contents/error', {title: `Erreur ${status}`, message: message});
}
