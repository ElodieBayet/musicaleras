'use strict';

/**
 * @file Middlewares for Infrastructure services : dynamic navigation, local values, etc.
 */

/**
 * Extract and store position in app
 */
exports.position = (req, res, next) => {

    res.locals.path = req.originalUrl.substr(1);
    res.locals.path = res.locals.path.split('/');

    next();
}