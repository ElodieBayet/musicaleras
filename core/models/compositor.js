'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Schema for Compositor Documents
 * @type {Schema}
 */
const compositorSchema = new Schema(
    {
        tag: {
            type: String,
            required: true,
            unique: true,
            lowercase: true
        },
        lastname: {
            type: String,
            required: true,
            minLength: 3,
            maxLength: 128,
            uppercase: true
        },
        firstname: {
            type: String,
            required: true,
            minLength: 3,
            maxLength: 128
        },
        birth: {
            type: Date,
            required: true
        },
        death: {
            type: Date,
            required: true
        },
        origin: {
            type: String,
            maxLength: 128
        },
        figure: {
            type: String,
            default: '/figures/default_640x640.jpg'
        },
        periods: [
            {
                type: Schema.Types.ObjectId,
                ref: 'Periods'
            }
        ] 
    },
    {
        id: false,
        toJSON: {
            virtuals: true
        }
    }
);

// Virtual for compositor's fullname
compositorSchema
    .virtual('name')
    .get(function () {
        return `${this.firstname} ${this.lastname}`;
    });

// Virtual for compositor's lifespan
compositorSchema
    .virtual('life')
    .get(function () {
        if (this.death && this.birth) return this.death.getFullYear() - this.birth.getFullYear();
        return;
    });

// Virtual for compositor's timelaps
compositorSchema
    .virtual('timelaps')
    .get(function () {
        if (this.death && this.birth) return `de ${this.birth.getFullYear()} à ${this.death.getFullYear()}`;
        return;
    });

/** Mongoose Modeling of a Compositor  */
const compositorModel = mongoose.model('Compositors', compositorSchema);

module.exports = { compositorSchema, compositorModel };