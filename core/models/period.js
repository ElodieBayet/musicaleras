'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Schema for Period Documents
 * @type {Schema}
 */
const periodSchema = new Schema(
    {
        tag: { 
            type: String,
            required: true,
            unique: true,
            lowercase: true
        },
        name: { 
            type: String,
            required: true,
            minLength: 3,
            maxLength: 128
        },
        begin: { 
            type: Number,
            required: true,
            min: 1, max: 2100
        },
        end: {
            type: Number,
            required: true,
            min: 1, max: 2100
        },
        description: {
            type: String,
            maxLength: 500
        }
    },
    {
        id: false,
        toJSON: {
            virtuals: true
        }
    }
);

// Virtual for a shorten description
periodSchema
    .virtual('shortened')
    .get(function () {
        if (this.description) return `${this.description.substr(0, 85)} ...`;
        return;
    });

// Virtual for period duration
periodSchema
    .virtual('duration')
    .get(function () {
        if (this.begin && this.end) return this.end - this.begin;
        return;
    });

// Virtual for readable period timelaps
periodSchema
    .virtual('timelaps')
    .get(function(){
        if (this.begin && this.end) return `de ${this.begin} à ${this.end}`;
        return;
    });

/** Mongoose Modeling of a Period */
const periodModel = mongoose.model('Periods', periodSchema);

module.exports = { periodSchema, periodModel };