'use strict';

/**
 * @file Utilities : functions
 */

/**
 * Intermediate Function for Inheritance
 * @param {Object} Parent 
 * @param {Object} Child 
 */
exports.extendPrototype = (Parent, Child) => {
    Child.prototype = Object.create(Parent.prototype);
    Child.prototype.constructor = Child;
}