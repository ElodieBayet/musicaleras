'use strict';
const { extendPrototype } = require('./utils');

/**
 * Customize classic Error for Http context. Extends Error object.
 * @param {number} status Http status code. I.e. 404
 * @param {string} message Message to customize
 */
function HttpException(status, message) {
    Error.call(this, message);
    this.status = status;
    this.message = message;
}

// Call Intermediate Function of Inheritance
extendPrototype(Error, HttpException);

exports.HttpException = HttpException;