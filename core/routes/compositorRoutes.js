'use strict';
const router = require('express').Router();
const compositor = require('../controllers/compositorController');

/**
 * @file Manage Routes for '/compositeurs' URIs
 */

router
    // GET :: Reading
    .get('/:tag', compositor.readOneCompositor)
    .get('/', compositor.readAllCompositors);

module.exports = router;