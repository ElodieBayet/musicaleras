'use strict';
const router = require('express').Router();
const common = require('../controllers/commonController');

/**
 * @file Manage routes for common URIs
 */

router
    // GET :: Reading
    .get('/infos', common.infos)
    .get('/', common.home)

module.exports = router;