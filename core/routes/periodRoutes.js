'use strict';
const router = require('express').Router();
const period = require('../controllers/periodController');

/**
 * @file Manage routes for '/periodes' URIs
 */

router
    // GET :: Reading
    .get('/:tag', period.readOnePeriod)
    .get('/', period.readAllPeriods);

module.exports = router;