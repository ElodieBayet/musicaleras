'use strict';

/**
 * @file Exports all routes
 */

module.exports.commons = require('./commonRoutes.js');
module.exports.asyncs = require('./asynchronousRoutes.js');
module.exports.periods = require('./periodRoutes.js');
module.exports.compositors = require('./compositorRoutes.js');