'use strict';
const router = require('express').Router();
const asynchronous = require('../controllers/asyncController');

/**
 * @file Manage routes for '/async' requests
 */

router
    // GET :: Reading
    .get('/chronographic/:tag', asynchronous.chronographic);

module.exports = router;