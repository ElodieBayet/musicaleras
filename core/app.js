'use strict';
const { join } = require('path');
const compression = require('compression');
const helmet = require('helmet');
const mongoose = require('mongoose');
const express = require('express');
const routes = require('./routes/collectionRoutes');
const { badRequest, fallout } = require('./services/http');
const { position } = require('./services/infrastructure');
const debug = require('debug')('core:app');

// Database connection 
mongoose
    .connect( process.env.MONGODB_URI )
    .then( () => debug(`(i) - Connected to DataBase`) )
    .catch( err => debug(`[!] - DataBase Connection Failed : ${err}`));

// Config & Setup 
const helmetOptions = {
    contentSecurityPolicy : { useDefaults: true, directives: { imgSrc: ['*'] } }
}
const app = express();
app
    .set('views', join(__dirname, 'views'))
    .set('view engine', 'pug');

// Routing & Middlewares
app
    .use( compression() )
    .use( express.static( join(__dirname, '..', 'public')) )
    .use( express.static( join(__dirname, '..', 'assets')) )
    .use( helmet( helmetOptions ) )
    .use( express.json() )
    .use( position )
    .use('/async', routes.asyncs)
    .use('/periodes', routes.periods)
    .use('/compositeurs', routes.compositors)
    .use('/', routes.commons)
    .use( badRequest )
    .use( fallout );

module.exports = app;