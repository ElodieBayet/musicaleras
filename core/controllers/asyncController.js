'use strict';
const debug = require('debug')('core:async');
const { periodModel } = require('../models/period');
const { compositorModel } = require('../models/compositor');

/**
 * Get all compositors for a period 
 * @param {*} req Http request with tag param
 * @param {*} res Http response
 * @returns Json object
 */
exports.chronographic = (req, res, next) => {

    periodModel
        .findOne({tag: req.params.tag}, {_id:true, tag:true})
        .then(period => {
            if (!period) throw new Error(`Cette période n'existe pas.`);

            return compositorModel
                .find({periods: period._id}, {lastname: true, firstname: true})
                .then(compositors => {
                    if (compositors.length === 0) throw new Error('Aucun.e compositeur.rice trouvé.e');
                
                    return res.status(200).json(compositors);
                } )
        } )
        .catch(error => {
            debug(`[!] - Chronographic :: ${error.message}`);
            error.message = process.env.NODE_ENV === 'development' ? error.message : `Aucun résultat pour cette requête.`;
            return res.status(404).json( {error: error.message} );
        });
}