'use strict';
const async = require('async');
const { HttpException } = require('../library/httpException');
const { periodModel } = require('../models/period');
const { compositorModel } = require('../models/compositor');

/**
 * Display details page of targeted compositor
 * * Get one compositor by 'tag' property with all fields, and provide prévious and next compositors
 * @param {*} req Http request to provide data identifier
 * @param {*} res Http response
 * @param {*} next Transmit an HttpException
 */
exports.readOneCompositor = (req, res, next) => {

    compositorModel
        .findOne({tag: req.params.tag})
        .populate('periods', ['name', 'tag'])
        .then(compositor => {
            if (!compositor) throw new Error(`Compositeur.rice non trouvé.e`);

            async.parallel(
                {
                    previousCompositor: (callback) => {
                        compositorModel
                            .find({birth: {$lt: compositor.birth}}, {tag:true, birth:true, lastname:true, firstname:true})
                            .sort([['birth', 'descending']])
                            .limit(1)
                            .exec(callback);
                    },
                    nextCompositor: (callback) => {
                        compositorModel
                            .find({birth: {$gt: compositor.birth}}, {tag:true, birth:true, lastname:true, firstname:true})
                            .sort([['birth', 'ascending']])
                            .limit(1)
                            .exec(callback);
                    }
                },
                (err, results) => {
                    if(err) throw new Error(err.message);

                    return res.render('contents/compositor', {
                        title: compositor.name,
                        description: `Présentation de ${compositor.name}`,
                        compositor: compositor,
                        previous: results.previousCompositor[0],
                        next: results.nextCompositor[0]
                    });
                }
            )
        })
        .catch(error => {
            error.message = process.env.NODE_ENV === 'development' ? error.message : `Aucune catégorie trouvée.`;
            return next(new HttpException(404, error.message));
        });
}

/**
 * Display "Compositeurs" page with a list of all compositors
 * * Get all compositors in birth date ascending w/ {tag, firstname, lastname, birth, death} fields only
 * @param {*} res Http response
 * @param {*} next Transmit an HttpException
 */
exports.readAllCompositors = (req, res, next) => {

    compositorModel
        .find({}, {tag: true, firstname: true, lastname: true, birth: true, death: true})
        .populate('periods', ['name', 'tag'])
        .sort([['birth', 'ascending']])
        .then(compositors => {
            if (compositors.length === 0) throw new Error(`Compositeur.rice non trouvé.e`);

            periodModel
                .find({}, {tag: true, name: true})
                .sort([['begin', 'ascending']])
                .then(periods => {
                    return res.render('contents/list-compositors', { 
                        title: 'Compositeurs',
                        description: 'Liste de quelques compositeur.rice.s en musique occidentale savante',
                        compositors: compositors,
                        periods: periods
                    });
                } )
        } )
        .catch(error => {
            error.message = process.env.NODE_ENV === 'development' ? error.message : `Aucune catégorie trouvée.`;
            return next(new HttpException(404, error.message));
        });
}