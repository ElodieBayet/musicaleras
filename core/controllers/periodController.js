'use strict';
const async = require('async');
const { HttpException } = require('../library/httpException');
const { periodModel } = require('../models/period');
const { compositorModel } = require('../models/compositor');

/**
 * Display details page of targeted period
 * * Get one period by 'tag' property with all fields, and provide prévious and next periods
 * @param {*} req Http request to provide data identifier
 * @param {*} res Http response
 * @param {*} next Transmit an HttpException
 */
exports.readOnePeriod = (req, res, next) => {

    periodModel
        .findOne({tag: req.params.tag})
        .then(period => {
            if (!period) throw new Error(`Période non trouvée`);

            async.parallel(
                {
                    previousPeriod: (callback) => {
                        periodModel
                            .find({begin: {$lt: period.begin}}, {tag:true, begin:true, name:true})
                            .sort([['begin', 'descending']])
                            .limit(1)
                            .exec(callback );
                    },
                    nextPeriod: (callback) => {
                        periodModel
                            .find({begin: {$gt: period.begin}}, {tag:true, begin:true, name:true})
                            .sort([['begin', 'ascending']])
                            .limit(1)
                            .exec(callback);
                    },
                    allCompositors: (callback) => {
                        compositorModel
                            .find({periods:period._id}, {tag:true, lastname:true, firstname:true, birth:true, death:true})
                            .exec(callback)
                    }
                },
                (err, results) => {
                    if (err) throw new Error(err.message);
                    
                    return res.render('contents/period', { 
                        title: period.name,
                        description: `Description sur la période musicale ${period.name}`,
                        period: period,
                        previous: results.previousPeriod[0], 
                        next: results.nextPeriod[0],
                        compositors: results.allCompositors
                    })
                }
            )
        } )
        .catch(error => {
            error.message = process.env.NODE_ENV === 'development' ? error.message : `Aucune période trouvée.`;
            return next(new HttpException(404, error.message));
        });
}

/**
 * Display "Periodes" page with a list of all periods
 * * Get all periods in begin date ascending w/ {tag, name, begin, end} fields only
 * * Count total of compositors for each period
 * @param {*} res Http response
 * @param {*} next Transmit an HttpException
 */
exports.readAllPeriods = (req, res, next) => {

    periodModel
        .find()
        .sort([['begin', 'ascending']])
        .then(periods => {

            if (periods.length === 0) throw new Error(`Aucune période trouvée.`);

            /**
             * Count quantity of sub-documents within its parent document
             * @param {Object} period Parent document
             * @param {Function} callback Function with 2 arguments: error and results
             */
            function counter(period, callback) {

                compositorModel.countDocuments( {periods: period._id}, (err, total) => {
                    if (err) return callback(err); 
                    // Rewrite current object with additional field
                    let result = {tag: period.tag, name: period.name, timelaps: period.timelaps, shortened: period.shortened, count: total};
                    callback(null, result);
                });
            }

            /**
             * Implement each period with total of compositors
             */
            async.map(periods, counter, (err, results) => {
                if (err) throw new Error(`[!] - Problème de sous-requête :`, err);

                return res.status(200).render('contents/list-periods', { 
                    title: `Périodes`,
                    description: `Liste des périodes prinicpales en Musique occidentale savante`,
                    periods: results
                })
            });

        } )
        .catch(error => {
            error.message = process.env.NODE_ENV === 'development' ? error.message : `Aucune période trouvée.`;
            return next(new HttpException(404, error.message));
        });
}