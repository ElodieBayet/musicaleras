'use strict';
const { HttpException } = require('../library/httpException');
const { periodModel } = require('../models/period');

/**
 * Display Home page
 * * Get all periods in ascending with {tag, name, begin, end} fields only
 * @param {*} res Http response
 * @param {*} next Transmit an HttpException
 * @returns View render with data or call next if DB reading failed
 */
exports.home = (req, res, next) => {

    periodModel
        .find({}, {tag: true, name: true, begin: true, end: true})
        .sort([['begin', 'ascending']])
        .then(periods => {
            return res.status(200).render('contents/home', { title: `Périodes en musique savante`, description: `Diagramme chronologique des périodes en musique occidentale savante`, periods: periods })
        })
        .catch(error => {
            error.message = process.env.NODE_ENV === 'development' ? error.message : `Aucune catégorie trouvée.`;
            next(new HttpException(404, error.message));
        });
}

/**
 * Display Informations page
 * @param {*} res Http response
 * @returns View render
 */
exports.infos = (req, res, next) => {

    return res.status(200).render('contents/info', { title: `Informations`, description: `Renseignements sur l'application et son contenu` })
}